from numpy import empty, shape


def copy_vector_to_vector(x, y):
    x_m, x_n = shape(x)
    y_m, y_n = shape(y)

    assert (x_m == 1 or x_n == 1) and (y_m == 1 or y_n == 1), \
        "FAILED, input variables are not vectors"

    if x_m is y_m:
        if x_m == 1:
            for position in xrange(x_m):
                y[0, position] = x[0, position]
        else:
            for position in xrange(x_n):
                y[position, 0] = x[position, 0]

    elif x_m is y_n:
        if x_m == 1:
            for position in xrange(x_m):
                y[0, position] = x[position, 0]
        else:
            for position in xrange(x_m):
                y[position, 0] = x[0, position]

    return y


def laff_scal(alpha, x):
    """
    Scales input vector with scalar alpha
    """
    try:
        x_m, x_n = shape(x)
    except:
        print("FAILED, x is not a matrix")
        raise

    assert x_n == 1 or x_m == 1, "FAILED, x is not a vector"

    assert type(alpha) == int, "FAILED, alpha is not a scalar"

    x_out = empty([x_m, x_n])
    if x_n > 1:
        counter_length = x_n
        for counter in range(counter_length):
            x_out[0, counter] = alpha * x[0, counter]
    else:
        counter_length = x_m
        for counter in range(counter_length):
            x_out[counter, 0] = alpha * x[counter, 0]

    return x_out
